<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">



<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" content="Eyolf &Oslash;strem" />
<title>The propelling harmony of &quot;Dear Landlord&quot;</title>
<link href="../css/general.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../css/articles.css" />

</head>

<body>
<div id="content">
<p><a href="../about.htm">
  </a></p>
  <h1>The propelling harmony of &quot;Dear Landlord&quot;</h1>
  <h2 class="author">Eyolf &Oslash;strem</h2>
  <p class="first"><span class="dropcap">W</span>hereas a considerable number of Dylan songs use only a limited number of chords, often
    the classic three-chord blues pattern, often just one sustained chord throughout a whole
    song (e.g. Political World), some songs stand out as far more advanced, harmonically
  speaking. I would like to discuss Dear Landlord from this perspective. </p>
  <h4>Level 1 - the Dry Description</h4>
  <p class="first">Dear Landlord begins with a C [<em>I will refer to major chords with upper-case letters
    alone, and minor chords with an added &quot;-m&quot;, e.g. Am</em>] held for most of the
    first line. At the end of this line (at &quot;soul&quot;) the harmony changes to E. The
    next line sees this E used as a dominant to a, which, by way of a standard bass-descent,
    reaches F. Again, after a short excursion to d, this chord is used as a new dominant, this
    time even as a seventh chord (F7), to the Bb of the third phrase. The last phrase leads
    the harmony back to d, where it seems to end, until the accented F and G chords bring back
  the C of the opening.</p>
  <pre class="verse quote">
D E A R   L A N D L O R D</pre>
  
<pre class="verse quote">
C                                             E
Dear landlord, Please don't put a price on my soul. 
Am                 F                Em      Dm
My burden is heavy, My dreams are beyond control. 
F                           F7 
When that steamboat whistle blows, 
Bb                       (Bb7)
I'm gonna give you all I got to give, 
    C    Dm7        C          Gm/bb
And I do hope you receive it well, 
   F                     Gm            Dm     F F   G
Dependin' on the way you feel that you live.</pre>
  
<h4>Level 2 - the Harmonic Analysis</h4>
  <p class="first">Now, this dry description of the basic harmonic outline of the song conceals a very
    dramatic line of events. The move from C to E is in fact a wide leap. Why this is so, can
    be explained in many ways. Traditional harmony considers chords to be arranged according
    to the <em>circle of fifths</em>, based on the understanding that chords a fifth apart (i.e.
    with the lowest notes separated by five steps) are more closely related than other chords.
    This gives two close relatives for each chord:&nbsp;a fifth above and a fifth below. The
    chord above is, for different reasons, the nearest relative, and is called the <em>dominant</em>,
    whereas the chord below is called the <em>subdominant</em>. A song in C will then have G as
    its dominant and F as subdominant, and there you are: the three chords upon which 80% of
    all blues, rock - and classical music - is based. The distance from C to E along the
    circle of fifths is four steps:&nbsp;C-G-D-A-E, which is a long way to go in a single
    leap. <br />
    Another reason for the wideness of the leap is that the <em>e </em>of the E chord is the
    third in the C chord. As such, it is the tone that defines the chord as a major chord -
    lower it one step and you get instead a minor chord, leave it out, and you get no chord at
    all, just an interval, a fifth. Thus the third is the most essential interval in
    traditional harmony. The variability of the third makes a chord based upon this tone
    somewhat unstable, in relation to the firm relations between C, G and F. <br />
    Thirdly the major chord over E contains two tones a half step removed from the
    corresponding tones of the C major chord:&nbsp;E - G sharp - B, as opposed to E - G - C.
    The half tone is the interval of suspense in traditional harmony - &quot;demanding&quot; a
    resolution back to where it came from. This is the <em>raison d'être</em>, so to speak, of
    the seventh chord: the G chord contains the leading note B of the C major chord, and this
    is further enforced by the &quot;extra&quot; tone f, the seventh, in the G7 chord, which
  is experienced as leading to the tone E in the C major chord.</p>
  <p>All these factors contribute to the experience of the passage from C major to E major
    as a wide leap, generating a certain tension, which &quot;needs&quot; a resolution. In
    this perspective the leap from C to E works as a harmonic &quot;motor&quot; for the music
    that follows. The E major chord immediately serves as a dominant chord for the A minor
    that follows. (It should be mentioned, at least in a parenthesis, that this chord sequence
    - C, E, Am - is very common in several genres of popular music. But usually the E is
    merely inserted between the C and its close relative, Am. What is special in this song, is
    that the E major chord is given a prominence of its own; not merely as a passing,
    preparatory chord, but as the goal of the first phrase. It is rather the Am that is a
    passing chord in the following sequence, to which I will return, if only I can get out of
  this parenthesis.)</p>
  <p>I will be more brief on the following:&nbsp;As I said in there (it is not really
    formally correct to refer to what has been said in parentheses, but that's how it is when
    you get stuck) the E may be seen as a preparatory chord to the Am that follows. But this
    Am is also just a passing chord, on the way &quot;down&quot; - harmonically speaking - to
  F and its relative Dm, where the second phrase ends (&quot;control&quot;). </p>
  <p>Compared with the high-point at E, we have now reached a more relaxed level, five steps
    down, so to speak. But now enters the hand of God:&nbsp;The F/Dm level of repose is turned
    into a level of tension by divine intervention, i.e. by the artist. The F of relaxation is
    turned into an F7 of renewed tension (the seventh makes the whole difference) and we
    tumble yet another staircase down to Bb. And as if that was not enough, the last process
    is (almost) repeated, when even the Bb is turned into a Bb7. The distinctive seventh note
    of the Bb7 chord, which Dylan reaches on &quot;got (to give)&quot;, is the tone Ab. On a
    piano this is the same key as the G# in the E major chord. In reality it is the farthest
    away you can get - 12 steps along the circle of fifths, which happens to make a full
    circle and get back to a tone that may <em>sound</em> the same, but which <em>feels</em> very
    different. (Time for a new parenthesis. It is only on a modern, equally tempered
    instrument like the piano that the tones Ab and G# sound the same. The wonderful mystery
    of the tonal system is that 12 steps in the circle of fifths do <em>not</em> bring one back
    to the same tone, but to a tone a little bit higher. On a piano this little interval has
    been divided in 12 and distributed among all the fifths, which means that any interval
    played on a piano is in fact out of tune . . . A singer, on the other hand, will usually
  bring out this difference, consciously or unconsciously.)</p>
  <p>To put it bluntly:&nbsp;going from E to Am is just to follow the call of nature, but to
    redefine F into F7 - that is a creative act. To repeat the act is daring, but to refrain
    from fufilling it, is just as daring; the Bb7 never reaches its &quot;natural&quot; goal,
    which would have been Eb. Instead we are left suspended in the air, until we somehow land
    on C, but still not, since the C is just one among many restless steps on the way down to
    the Dm which seemingly ends the last phrase. At this point it may be correct to say that
    so much has happened in the meantime, that one really doesn't remember the sound of the C
    that started it all. The C major chord has appeared here and there throughout the song,
    but always in passing, never as the basic tonality upon which everything rests. Now this C
    is evoked again through the abrupt hammering of F and G, which rashly sweeps away
    everything that has happened and very emphasisedly re-establishes C as the tonal centre
  again.</p>
  <h4>Level 3 - the interpretation</h4>
  <p class="first">What's happening in here, then?&nbsp;I'm not about to argue that the E in the first
    phrase by necessity leads to everything that follows. Nor, actually, that it is a
    successful song - maybe the hand of God is too daring, maybe the return from Bb to C is
    too blunt, and maybe the final directionless descent to Dm and the following return to C
    is too contrieved, I really don't know. What I know is that the song and touches me,
    precisely because of these things. What I've tried to do above, is to explain, in
    harmonical terms, why it is that the tremendous burst of energy which I sense in the song,
  ultimately feels so tired.</p>
  <hr />
</div>
</body>
</html>